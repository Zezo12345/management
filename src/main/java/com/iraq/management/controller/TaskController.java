package com.iraq.management.controller;

import com.iraq.management.request.TaskRequest;
import com.iraq.management.response.GeneralResponse;
import com.iraq.management.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/task")
public class TaskController {

    @Autowired
    TaskService taskService;

    @GetMapping("/getAllTasks")
    public GeneralResponse<?> getAllTasks () {

        return taskService.getAllTasks();
    }
    @GetMapping("/getASingleTask/{id}")
    public GeneralResponse<?> getTaskById (@PathVariable("id") Long id) {

        return taskService.getASingleTask(id);
    }
    @PostMapping("/createTask")
    public GeneralResponse<?> createTask( @RequestBody TaskRequest taskRequest) {

        return  taskService.createTask(taskRequest);
    }

    @PutMapping("/updateTask")
    public GeneralResponse<?> updateTask(@RequestBody TaskRequest taskRequest) {
        return taskService.updateTask(taskRequest);

    }

    @DeleteMapping("/deleteTask/{id}")
    public GeneralResponse<?> deleteEmployee(@PathVariable("id") Long id) {

        return taskService.deleteTask(id);
    }

}
