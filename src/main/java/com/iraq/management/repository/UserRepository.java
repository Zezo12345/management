package com.iraq.management.repository;



import java.util.Optional;

import com.iraq.management.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	 @Query("select u from User u where username=:username")
	  Optional<User> findByUsername(@Param(value = "username") String username);

	  Boolean existsByUsername(String phone);

	  Boolean existsByEmail(String email);
}
