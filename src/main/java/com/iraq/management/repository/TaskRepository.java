package com.iraq.management.repository;
import com.iraq.management.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TaskRepository  extends JpaRepository<Task, Long> {

    Optional<Task>  findByTitle(String title);
}
