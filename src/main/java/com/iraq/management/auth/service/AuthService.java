package com.iraq.management.auth.service;

import com.iraq.management.request.LoginRequest;
import com.iraq.management.request.SignupRequest;
import com.iraq.management.request.TokenRefreshRequest;
import com.iraq.management.response.GeneralResponse;
import jakarta.validation.Valid;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

@Component
public interface AuthService {
    public GeneralResponse<?> registerUser(SignupRequest signUpRequest);
    public GeneralResponse<?> authenticateUser(LoginRequest loginRequest);
    public GeneralResponse<?> refreshtoken(TokenRefreshRequest request);
}
