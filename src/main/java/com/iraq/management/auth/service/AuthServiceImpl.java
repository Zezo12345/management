package com.iraq.management.auth.service;

import com.iraq.management.auth.enumm.ERole;
import com.iraq.management.auth.jwt.JwtUtils;
import com.iraq.management.model.RefreshToken;
import com.iraq.management.model.Role;
import com.iraq.management.model.User;
import com.iraq.management.repository.RoleRepository;
import com.iraq.management.repository.UserRepository;
import com.iraq.management.request.LoginRequest;
import com.iraq.management.request.SignupRequest;
import com.iraq.management.request.TokenRefreshRequest;
import com.iraq.management.response.GeneralResponse;
import com.iraq.management.response.JwtResponse;
import com.iraq.management.response.MessageResponse;
import com.iraq.management.response.TokenRefreshResponse;
import com.iraq.management.service.RefreshTokenService;
import com.iraq.management.service.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class AuthServiceImpl implements AuthService{

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    RefreshTokenService refreshTokenService;



    public GeneralResponse<?> authenticateUser(LoginRequest loginRequest) {
        try {
            Optional<User> user=userRepository.findByUsername(loginRequest.getUsername());
            if(user.isEmpty()){
                return new GeneralResponse<>(205, new MessageResponse("username not found"));
            }
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
            SecurityContextHolder.getContext().setAuthentication(authentication);

            UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

            String jwt = jwtUtils.generateJwtToken(userDetails);

            List<String> roles = userDetails.getAuthorities().stream().map(item -> item.getAuthority())
                    .collect(Collectors.toList());

            RefreshToken refreshToken = refreshTokenService.createRefreshToken(userDetails.getId());
            return new GeneralResponse<>(200, new JwtResponse(jwt, refreshToken.getToken(), userDetails.getId(),

                    userDetails.getUsername(), userDetails.getEmail(),  userDetails.getName(), roles));

        }catch(Exception e) {
            return new GeneralResponse<>(209,e.getMessage());
        }


    }


    public GeneralResponse<?> registerUser(SignupRequest signUpRequest) {

        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            return new GeneralResponse<>(203, new MessageResponse("Error: Username is already taken!"));
        }
        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            return new GeneralResponse<>(204, new MessageResponse("Error: Email is already in use!"));
        }

        User user = new User(signUpRequest.getUsername(), signUpRequest.getEmail(),
                encoder.encode(signUpRequest.getPassword()), signUpRequest.getName());
        Set<String> strRoles = signUpRequest.getRole();
        Set<Role> roles = new HashSet<>();

        if (strRoles == null) {
            Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        } else {
            strRoles.forEach(role -> {
                switch (role) {
                    case "admin":
                        Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(adminRole);

                        break;
                    case "employee":
                        Role modRole = roleRepository.findByName(ERole.ROLE_EMPLOYEE)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(modRole);

                        break;
                    default:
                        Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(userRole);
                }
            });
        }

        user.setRoles(roles);
        userRepository.saveAndFlush(user);

        return new GeneralResponse<>(200, user);
    }

    public GeneralResponse<?> refreshtoken(TokenRefreshRequest request) {
        String requestRefreshToken = request.getRefreshToken();
        return refreshTokenService.findByToken(requestRefreshToken).map(refreshTokenService::verifyExpiration)
                .map(RefreshToken::getUser).map(user -> {
                    String token = jwtUtils.generateTokenFromUsername(user.getUsername());
                    return new GeneralResponse<>(200, new TokenRefreshResponse(token, requestRefreshToken));
                }).orElseGet(() -> new GeneralResponse<>(203,
                        new TokenRefreshResponse(requestRefreshToken, "Refresh token is not in database!")));
    }

}


