package com.iraq.management.auth.enumm;

public enum ERole {
  ROLE_USER,
  ROLE_EMPLOYEE,
  ROLE_ADMIN
}
