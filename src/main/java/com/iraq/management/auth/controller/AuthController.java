package com.iraq.management.auth.controller;
import com.iraq.management.auth.enumm.ERole;
import com.iraq.management.auth.jwt.JwtUtils;
import com.iraq.management.auth.service.AuthService;
import com.iraq.management.model.RefreshToken;
import com.iraq.management.model.Role;
import com.iraq.management.model.User;
import com.iraq.management.repository.RoleRepository;
import com.iraq.management.repository.UserRepository;
import com.iraq.management.request.LoginRequest;
import com.iraq.management.request.SignupRequest;
import com.iraq.management.request.TokenRefreshRequest;
import com.iraq.management.response.GeneralResponse;
import com.iraq.management.response.JwtResponse;
import com.iraq.management.response.MessageResponse;
import com.iraq.management.response.TokenRefreshResponse;
import com.iraq.management.service.RefreshTokenService;
import com.iraq.management.service.UserDetailsImpl;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    AuthService authService;

    @PostMapping(value = "/login", consumes = "application/json")
    public GeneralResponse<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        return authService.authenticateUser(loginRequest);
    }

    @PostMapping(value = "/register", consumes = "application/json")
    public GeneralResponse<?> registerUser(@RequestBody SignupRequest signUpRequest) {
        return authService.registerUser(signUpRequest);
    }

    @PostMapping(value = "/refreshtoken", consumes = "application/json")
    public GeneralResponse<?> refreshtoken(@Valid @RequestBody TokenRefreshRequest request) {
        return authService.refreshtoken(request);
    }
}
