package com.iraq.management.service;

import com.iraq.management.model.Task;
import com.iraq.management.repository.TaskRepository;
import com.iraq.management.request.TaskRequest;
import com.iraq.management.response.GeneralResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TaskServiceImpl implements TaskService{

    @Autowired
    TaskRepository taskRepository;
    @Override
    public GeneralResponse<?> createTask(TaskRequest taskRequest) {
        try {
            Task newTask = new Task();
            newTask.setDescription(taskRequest.getDescription());
            newTask.setTitle(taskRequest.getTitle());
            Task task=taskRepository.save(newTask);
            return new GeneralResponse<>(200,task);
        }catch(Exception e) {
            return new GeneralResponse<>(209,e.getMessage());
        }
    }

    @Override
    public GeneralResponse<?> updateTask(TaskRequest taskRequest) {

        try {
            Optional<Task> task = taskRepository.findById(taskRequest.getId());
            if (task.isEmpty()){
                return new GeneralResponse<>(205,"Task not found");
            } else {
                Task updatedTask = task.get();
                updatedTask.setTitle(taskRequest.getTitle());
                updatedTask.setDescription(taskRequest.getDescription());
                Task newTask = taskRepository.save(updatedTask);
                return new GeneralResponse<>(200, newTask);
            } }catch(Exception e) {
            return new GeneralResponse<>(209,e.getMessage());
        }
    }

    @Override
    public GeneralResponse<?> deleteTask(Long taskId) {
        try {
            if (taskRepository.findById(taskId).get().getId().equals(taskId)){
                taskRepository.deleteById(taskId);
            }
            return new GeneralResponse<>(200,"deleted Successfully");

        }catch(Exception e) {
            return new GeneralResponse<>(209,e.getMessage());
        }
    }

    @Override
    public GeneralResponse<?> getASingleTask(Long taskId) {
        try {
            Task task=taskRepository.findById(taskId).get();
            return new GeneralResponse<>(200,task);
        }catch(Exception e) {
            return new GeneralResponse<>(209,e.getMessage());
        }
    }

    @Override
    public GeneralResponse<?> getAllTasks() {
        try {
            List<Task> tasks= taskRepository.findAll();
            return new GeneralResponse<>(200,tasks);
        }catch(Exception e) {
            return new GeneralResponse<>(209,e.getMessage());
        }
    }
}
