package com.iraq.management.service;

import com.iraq.management.model.Task;
import com.iraq.management.request.TaskRequest;
import com.iraq.management.response.GeneralResponse;
import org.springframework.stereotype.Component;

@Component
public interface TaskService {

    GeneralResponse<?> createTask(TaskRequest taskRequest);
    GeneralResponse<?> updateTask(TaskRequest taskRequest);
    GeneralResponse<?> deleteTask(Long taskId);
    GeneralResponse<?> getASingleTask(Long taskId);
    GeneralResponse<?> getAllTasks();
}
