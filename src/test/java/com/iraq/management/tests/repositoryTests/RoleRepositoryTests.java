package com.iraq.management.tests.repositoryTests;

import com.iraq.management.auth.enumm.ERole;
import com.iraq.management.model.Role;
import com.iraq.management.repository.RoleRepository;
import com.iraq.management.repository.UserRepository;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


    @DataJpaTest
    @AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
    @TestMethodOrder(MethodOrderer.OrderAnnotation.class)
    @Rollback(false)
    public class RoleRepositoryTests {
        @Autowired
        private UserRepository repoUser;
        @Autowired
        private RoleRepository repoRole;

        @Test
        public void testCreateRoles() {
            Role admin = new Role(ERole.ROLE_ADMIN);
            Role employee = new Role(ERole.ROLE_EMPLOYEE);
            Role user = new Role(ERole.ROLE_USER);

            repoRole.saveAllAndFlush(List.of(admin,employee, user));
            long count = repoRole.count();
            assertEquals(3, count);
        }

    }

