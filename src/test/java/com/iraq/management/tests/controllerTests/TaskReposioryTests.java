package com.iraq.management.tests.controllerTests;

import com.iraq.management.model.Task;
import com.iraq.management.repository.TaskRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;
import java.util.List;
import java.util.Optional;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Rollback(false)
class TaskRepositoryTests {

    @Autowired
    private TaskRepository taskRepository;

    // JUnit test for saveEmployee
    @Test
    @Order(1)
    @Rollback(value = false)
    public void createTaskTest(){

        Task task = Task.builder()
                .title("First task")
                .description("Create the first task")
                .build();

        taskRepository.save(task);

        Assertions.assertThat(task.getId()).isGreaterThan(0);
    }

    @Test
    @Order(2)
    public void getEmployeeTest(){

        Task task = taskRepository.findById(1L).get();

        Assertions.assertThat(task.getId()).isEqualTo(1L);

    }

    @Test
    @Order(3)
    public void getListOfTasksTest(){

        List<Task> tasks = taskRepository.findAll();

        Assertions.assertThat(tasks.size()).isGreaterThan(0);

    }

    @Test
    @Order(4)
    @Rollback(value = false)
    public void updateEmployeeTest(){

        Task task = taskRepository.findById(1L).get();

        task.setTitle("First title Update");

        Task taskUpdated =  taskRepository.save(task);

        Assertions.assertThat(taskUpdated.getTitle()).isEqualTo("First title Update");

    }

    @Test
    @Order(5)
    @Rollback(value = false)
    public void deleteTaskTest(){

        Task task = taskRepository.findById(1L).get();

        taskRepository.delete(task);

        //employeeRepository.deleteById(1L);

        Task task1 = null;

        Optional<Task> optionalTask = taskRepository.findByTitle("First title Update");

        if(optionalTask.isPresent()){
            task1 = optionalTask.get();
        }

        Assertions.assertThat(task1).isNull();
    }

}
